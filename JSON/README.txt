
Already this project comes with rapidJson linked to the project's libraries.
But in case you do not know how to do it, I take the following steps:
1. Download the .zip from the repository.
https://github.com/Tencent/rapidjson
2. Uncompress it.
3. Find the include / rapidjson folder
4. Copy the folder rapidjson in your project.
5. Right click on the folder and select Mark directory as -> External library.
6. Enjoy.
